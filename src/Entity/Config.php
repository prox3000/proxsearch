<?php declare(strict_types=1);
/**
 * Copyright (c) 2019 Oleg Abramov
 */
 

namespace prox3000\proxsearch\Entity;

use prox3000\proxsearch\MorphologyProcessor\MorphologyProcessorInterface;
use prox3000\proxsearch\Storage\StorageInterface;

class Config
{


    /**
     * @var MorphologyProcessorInterface
     */
    protected $morphologyProcessor;
    /**
     * @var StorageInterface
     */
    protected $storage;


    /**
     * @return MorphologyProcessorInterface
     */
    public function getMorphologyProcessor(): MorphologyProcessorInterface
    {
        if (null === $this->morphologyProcessor) {
            throw new \Exception('Morphology Processor not defined');
        }

        return $this->morphologyProcessor;
    }

    /**
     * @param MorphologyProcessorInterface $morphologyProcessor
     *
     * @return Config
     */
    public function setMorphologyProcessor(MorphologyProcessorInterface $morphologyProcessor): Config
    {
        $this->morphologyProcessor = $morphologyProcessor;

        return $this;
    }

    /**
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface
    {
        if (null === $this->storage) {
            throw new \Exception('Storage not defined');
        }

        return $this->storage;
    }

    /**
     * @param StorageInterface $storage
     *
     * @return Config
     */
    public function setStorage(StorageInterface $storage): Config
    {
        $this->storage = $storage;

        return $this;
    }
}
