<?php declare(strict_types=1);
/**
 * Copyright (c) 2019 Oleg Abramov
 */

namespace prox3000\proxsearch\MorphologyProcessor;

/**
 * Interface MorphologyProcessorInterface
 */
interface MorphologyProcessorInterface
{

    /**
     * @param string $word
     *
     * @return string
     */
    public static function apply(string $word): string;
}
