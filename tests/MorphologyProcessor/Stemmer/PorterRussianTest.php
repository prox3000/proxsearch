<?php declare(strict_types=1);
/**
 * Copyright (c) 2019 Oleg Abramov
 */

namespace prox3000\proxsearch\Tests\MorphologyProcessor\Stemmer;

use prox3000\proxsearch\MorphologyProcessor\Stemmer\PorterRussian;
use PHPUnit\Framework\TestCase;

class PorterRussianTest extends TestCase
{
    public function testApply()
    {
        $voc = new \SplFileObject(__DIR__ . '/Russian/voc.txt');
        $output = new \SplFileObject(__DIR__ . '/Russian/output.txt');

        while (!$voc->eof()) {
            $this->assertEquals(
                trim($output->fgets()),
                trim(PorterRussian::apply($voc->fgets()))
            );
        }
    }
}
